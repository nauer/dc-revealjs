FROM node:stretch-slim

RUN apt-get update && apt-get install -y git \
 && rm -rf /var/lib/apt/lists/* \
 && git clone https://github.com/hakimel/reveal.js.git
WORKDIR /reveal.js
RUN git pull \
 && npm install \
 && npm audit fix
ADD highlight.tar.gz plugin
CMD npm start
