# Revealjs Docker-Compose

## Requirements
* git
* Docker >= 19.03.12
* Docker-compose >= 1.26.2

## Installation
~~~bash
git clone https://gitlab.com/nauer/dc-revealjs.git
cd dc-revealjs
docker build -t nauer/revealjs .
~~~

## Run
~~~bash
cd dc-revealjs
docker_compose up

firefox localhost:8000
~~~

## Edit presentation
Changes have to be done in the `index.html` file. Images have to be in the
**images** folder. CSS files have to be in the **css** folder.

## Create new presentation
* Copy `docker-compose.yml` file into the new project folder
* Create a **css** and **images** folder
* Add a new revealjs `index.html` file or copy `index.html` from **dc-revealjs**
folder into the new project folder
* Run `docker-compose up` in the new project folder
* Update `index.html`
